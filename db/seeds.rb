# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

User.create!(name:  "Stefan Helber",
            email: "stefan.helber@prod.uni-hannover.de",
            password:              "geheim",
            password_confirmation: "geheim",
            admin: true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

S1 = Site.create!(name: "Institut für Arbeitsökonomik")
S2 = Site.create!(name: "Institut für Produktionswirtschaft")
S3 = Site.create!(name: "Institut für Controlling")
S4 = Site.create!(name: "Institut für Marketing")
S5 = Site.create!(name: "Institut für Rechnungswesen")
S6 = Site.create!(name: "Institut für Statistik")
S7 = Site.create!(name: "Institut für Gesundheitsökonomie")
S8 = Site.create!(name: "Institut für Wirtschaftsinformatik")


(10..99).each do |n|
  name = "Beispielinstitut-#{n}"
  codename = "C#{n}"
  Site.create!(name: name)
end
