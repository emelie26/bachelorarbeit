class CreateInstitutes < ActiveRecord::Migration[5.1]
  def change
    create_table :institutes do |t|
      t.string :name
      t.integer :Anzahl_Professoren
      t.integer :Anzahl_Mitarbeiter
      t.float :Betreuungsüberschusskapazität

      t.timestamps
    end
  end
end
