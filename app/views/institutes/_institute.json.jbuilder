json.extract! institute, :id, :name, :Anzahl_Professoren, :Anzahl_Mitarbeiter, :Betreuungsüberschusskapazität, :created_at, :updated_at
json.url institute_url(institute, format: :json)
