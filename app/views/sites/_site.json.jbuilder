json.extract! site, :id, :name, :codename, :created_at, :updated_at
json.url site_url(site, format: :json)
