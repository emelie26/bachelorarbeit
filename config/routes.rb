Rails.application.routes.draw do
  resources :students
  resources :institutes
  resources :sites
  root   'static_pages#home'
  get    '/help', to: 'static_pages#help'
  get    '/about', to: 'static_pages#about'
  get    '/contact', to: 'static_pages#contact'
  get    '/signup', to: 'users#new'
  get    '/login', to: 'sessions#new'
  get    'matching_start', to: 'static_pages#matching_start'
  post   '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users
end
